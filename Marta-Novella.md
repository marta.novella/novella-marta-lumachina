# Marta Novella

## "Git gud with Git: una lista divertente di comandi essenziali"

---
### Inizializzare un repository
 git init: inizializza un repository Git vuoto nella directory corrente o in quella specificata. In pratica, crea la cartella .git che contiene tutti i file e le informazioni necessarie per tracciare le modifiche ai file nella directory.

**Esempi di utilizzo:** 
mkdir myproject
cd myproject
git init

 Dopo aver eseguito il comando git init, verrà creata una nuova directory nascosta denominata .git, che conterrà tutti i file e le informazioni necessarie per gestire il repository Git. 
Alcuni dei file più importanti all'interno della directory .git includono: 

head: un file che punta al branch attuale
config: un file di configurazione per il repository Git
objects: una directory che contiene gli oggetti Git (commit, alberi e blob)
refs: una directory che contiene i riferimenti (branches, tags e remote-tracking branches)

---

### Configurare il nome e l'email dell'autore
git config: configurare le impostazioni di Git a livello locale o globale. È possibile configurare varie opzioni, tra cui il nome e l'email dell'autore, l'editor di testo predefinito, le credenziali di autenticazione, le preferenze di formattazione del testo e altro ancora.

**Esempi di utilizzo:** 

Impostare il nome dell'autore a livello globale
git config --global user.name "Mario Rossi" 

Impostare l'email dell'autore a livello globale
git config --global user.email "marta.nove@email.com"

Impostare l'editor di testo predefinito a livello globale
git config --global core.editor "nano"


Visualizzare tutte le impostazioni configurate a livello globale
// le opzioni configurate a livello globale sono applicate a tutti i repository Git sul computer
git config --global --list


Visualizzare tutte le impostazioni configurate a livello locale
// le opzioni configurate a livello locale sono specifiche per il repository corrente
git config --list


---

### copia un repository su una nuova directory locale
git clone: copia un repository Git esistente su una nuova directory locale. Questo comando è utile quando si lavora in un team e si vuole ottenere una copia del repository su un'altra macchina. 
Il comando git clone richiede l'URL del repository remoto.

**Esempi di utilizzo:** 

copia un repository su una nuova directory locale
// il repository remoto verrà copiato nella directory di destinazione specificata e sarà pronto per l'utilizzo

git clone https://github.com/Marta_Novella/nome_repository.git

Dopo aver eseguito il comando git clone, il repository remoto verrà copiato nella directory di destinazione specificata e sarà pronto per l'utilizzo. È possibile apportare modifiche ai file, eseguire il commit delle modifiche e quindi eseguire il push delle modifiche sul repository remoto.
---

### Aggiungere i file al index
git add: viene utilizzato per aggiungere i file modificati o nuovi al cosiddetto "index" di Git, preparandoli per essere inclusi nel prossimo commit.
**Esempi di utilizzo:** 

Aggiungere tutti i file modificati o nuovi al "index":
git add .

Aggiungere un file specifico al "index":
git add path/to/file

Aggiungere tutti i file in una determinata directory al "index":
git add path/to/directory/

È possibile eseguire il comando git status per verificare lo stato del repository e vedere quali file sono stati modificati o aggiunti alla "index".
git add è un'operazione fondamentale nel workflow di Git e deve essere utilizzato regolarmente per tenere traccia delle modifiche ai file.
---

### Effettuare un commit
git commit: registra i cambiamenti selezionati nella zona di staging come un nuovo commit nella storia del repository. In pratica, si sta creando un punto di ripristino che include le modifiche ai file selezionati. 
Il comando git commit richiede una descrizione del commit e deve essere breve descrizione delle modifiche apportate.
Dopo aver eseguito il comando git commit, il nuovo commit verrà creato e verrà inclusa una descrizione delle modifiche apportate ai file. Il commit rappresenta un'istantanea del repository in quel momento, che può essere utilizzata per ripristinare lo stato del repository in caso di problemi o per tenere traccia delle modifiche nel tempo.

**Esempi di utilizzo:** 
git commit -m "Descrizione del commit"

E’ possibile utilizzare l'opzione -a con il comando git commit per aggiungere automaticamente tutti i file modificati all'index e creare un nuovo commit 
git commit -a -m "Descrizione del commit"

Nota: Con Git, se sbagli una volta, puoi sempre riprovare fino a che non funziona. E poi sperare che i tuoi colleghi non vedano il tuo commit history.
---

### Caricare le modifiche su un server remoto
git push: Il comando git push richiede l'URL del repository remoto, che di solito viene denominato "origin", e il nome del ramo locale che si desidera caricare nel repository remoto. Il comando caricherà tutti i commit locali che non sono ancora presenti nel repository remoto.

**Esempi di utilizzo:** 
git push origin branch
// Origin specifica il nome del repository remoto e l'opzione branch specifica il nome del ramo locale che si desidera caricare nel repository remoto. 
---

### Scaricare le modifiche da un server remoto
git pull: scarica tutti i commit del branch remoto specificato e li incorpora nel branch locale corrente. Questo comando è utile per sincronizzare la propria copia locale del repository con le modifiche apportate da altri membri del team. 

**Esempi di utilizzo:** 
git pull origin branch
// Il comando recupererà tutte le modifiche dal repository remoto e le applicherà al branch corrente.
---

### mostrare lo stato attuale del repository
git status: mostra lo stato attuale del repository, incluso lo stato dei file, la posizione del branch e altro ancora. In pratica, si sta controllando quali file sono stati modificati, quali sono nella zona di staging e quali sono già stati committati. 

**Esempi di utilizzo:** 
Il comando visualizza uno stato simile al seguente:

avrà un output:
 
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   file1.txt
        modified:   file2.txt

no changes added to commit (use "git add" and/or "git commit -a")

---

### Visualizzare la storia dei commit
git log: mostra la storia dei commit del repository. In pratica, si sta visualizzando un elenco di tutti i commit effettuati nel repository, con informazioni come il messaggio di commit, la data e l'autore. 
**Esempi di utilizzo:** 

Il comando visualizza la cronologia dei commit simile al seguente:

commit 368ce64e7d6656d1ed6c2128198fc693ddcc769d (HEAD -> main)
Author: John Smith <john.smith@example.com>
Date:   Thu Apr 29 12:34:56 2021 -0400

    Updated file1.txt

commit 5c5e5ef09d9f20f91a2b4d4c266b831f4c4e9b83
Author: Jane Doe <jane.doe@example.com>
Date:   Wed Apr 28 09:12:34 2021 -0400

    Added file2.txt

commit 89c2713d3a08f7e2ed2f4470f6a0e6d9c19f035b
Author: John Smith <john.smith@example.com>
Date:   Tue Apr 27 14:56:34 2021 -0400
   
    Initial commit

git log elenca i tre commit effettuati nel repository.
Ogni commit è elencato in ordine cronologico inverso.

---

### Creare un branch
git branch: mostra tutti i branch presenti nel repository e indica quale branch è attualmente attivo. Questo comando è utile per controllare a quale branch ci si trova e quali sono disponibili. 

**Esempi di utilizzo:**

Per elencare tutte le branch disponibili nel repository:
Questo comando visualizza un elenco di tutte le branch nel repository:
git branch 

Per creare una nuova branch:
Questo comando creerà una nuova branch chiamata "nuova-branch" a partire dalla branch attuale.
git branch nuova-branch

Per spostarsi su una branch esistente:
Questo comando sposterà il repository sulla branch chiamata "nome-branch".
git checkout nome-branch 

Per creare una nuova branch e spostarsi su di essa contemporaneamente:
creerà una nuova branch chiamata "nuova-branch" a partire dalla branch attuale e sposterà il repository sulla nuova branch.
git checkout -b nuova-branch

Per eliminare una branch:
Questo comando elimina la branch chiamata "nome-branch".
questo comando elimina la branch solo se tutti i commit della branch sono stati integrati in altre branch, altrimenti Git mostrerà un errore e non eliminerà la branch.
git branch -d nome-branch
---

### Spostarsi su un altro branch
git checkout: sposta la testa del branch sul commit specificato o su un branch diverso. In pratica, si sta spostando la propria posizione nel repository su un altro commit o branch. 

**Esempi di utilizzo:** 
Per spostarsi sulla branch chiamata "nome-branch":
sposterà il repository sulla branch chiamata "nome-branch".
git checkout nome-branch

---

### incorporare cambiamenti di un altro branch in branch corrente
git merge: incorpora i cambiamenti di un altro branch nel branch corrente. Questo comando è utile quando si vuole unire le modifiche di un branch di lavoro in un altro. 

**Esempi di utilizzo:** 
Per unire la branch chiamata "nome-branch" alla branch attiva:
Questo comando unirà la branch chiamata "nome-branch" alla branch attiva del repository.
git merge nome-branch

Dopo aver eseguito il merge, Git può presentare conflitti tra le branch. In tal caso, è necessario risolvere manualmente i conflitti prima di eseguire un nuovo commit.

Nota: Non ci sono problemi con Git, solo merge conflict.
Nota 2 : C'è solo una cosa peggiore di un merge conflict in Git: due merge conflict in Git.
---

### chiavi SSH sono un metodo di autenticazione sicuro per accedere a un repository Git.
Queste chiavi consentono di autenticarsi senza la necessità di inserire ogni volta la password, rendendo più veloce e sicura l'accesso al repository.

Per utilizzare le chiavi SSH con Git, è necessario generare una coppia di chiavi SSH, composta da una chiave pubblica e una chiave privata. 
La chiave pubblica viene quindi associata al proprio account Git, 
mentre la chiave privata viene utilizzata per autenticarsi durante l'accesso al repository.

Generare la coppia di chiavi SSH utilizzando il comando ssh-keygen:
ssh-keygen

copiare la chiave pubblica generata (di solito salvata in ~/.ssh/id_rsa.pub).

Con le chiavi SSH configurate correttamente, Git utilizzerà automaticamente la chiave privata per autenticarsi durante l'accesso al repository, senza richiedere la password. 
Questo rende l'accesso al repository più veloce e sicuro.

---

